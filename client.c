#include <sys/types.h>
#include <sys/socket.h> 
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <setjmp.h>


#define MAX_MES_SIZE 1000
#define MAX_NAME 30

sigjmp_buf interrupt;

void SigHand(int s){
	printf("{CLIENT} Client stopped.\n");
	siglongjmp(interrupt,1);
}

int main(){
	signal(SIGINT,SigHand);  //для Ctrl+C 
	signal(SIGTSTP,SigHand); // для Ctrl+Z 
	int port, c, i, sch;
	char name[MAX_NAME+1];
	char mes[MAX_MES_SIZE+1];
	char b_mes[MAX_MES_SIZE+30];
	struct sockaddr_in addr;
	if (sigsetjmp(interrupt,1))
		return 0;
	int loc_sock=socket(AF_INET, SOCK_STREAM, 0);
	if (sigsetjmp(interrupt,1))
	{
		if (loc_sock!=-1) 
			shutdown(loc_sock, 2);;
		return 0;
	}
	if (loc_sock==-1)
	{
		perror("Error1");
		exit(0);
	}
	printf("Enter the port number: ");
	scanf("%d", &port);
	getchar();
	addr.sin_family = AF_INET; 
	addr.sin_port = htons(port); 
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	printf("%s", "\033[1;31m");
	printf("Welcome to the chatroom!\n");
	printf("(The message can contain no more than 1000 characters)\n");
	printf("%s", "\x1B[37m");
	fflush(stdout);
	MET:
	if (connect(loc_sock,(struct sockaddr *) &addr, sizeof(addr))<0)
	{
		perror("Error2");
		exit(1);
	}
	MET2:
	for (i=0; i<MAX_NAME; i++)
		name[i]=NULL;
	printf("Enter your nickname (at most 30 symbols): ");
	i=0;
	sch=0;
	while(((c=getchar())!='\n') && (i<=MAX_NAME)){
		if (c!=' ')
		{
			name[i++]=c;
			sch=0;
		}
		else if (i==0)
			continue;
		else if (sch==0)
		{
			name[i++]=c;
			sch++;
		}
	}
	if(i==MAX_NAME+1 || (i==1 && name[0]==' ') || (i==1 && name[0]=='\t') || i==0)
	{
		while((c=getchar()!='\n'));
		printf("Sorry, bad name!\n");
		goto MET2;
	}
	if(name[i-1]==' ')
		name[i-1]='\0';
	else
		name[i]='\0';
	send(loc_sock, name, strlen(name)+1, 0);
	for (i=0; i<MAX_MES_SIZE; i++)
				b_mes[i]=NULL;
	recv(loc_sock, b_mes, MAX_MES_SIZE+30, 0);
	if(strcmp(b_mes, "{SERVER} Sorry, this name allready used in chat. Choose another one.")==0)
	{
		printf("%s\n", b_mes);
		close(loc_sock);
		goto MET;		
	}
	printf("%s\n",b_mes);
	while(1){
		fd_set readfds;
		int max_d=loc_sock;
		FD_ZERO(&readfds);
		FD_SET(0,&readfds); 
		FD_SET(loc_sock, &readfds);
		int res=select(max_d+1, &readfds, NULL, NULL, NULL);
		if(res<=0)
		{	
			perror("Error3");
			exit(3);
		}
		if(FD_ISSET(loc_sock, &readfds))
		{
			for (i=0; i<MAX_MES_SIZE+30; i++)
				b_mes[i]=NULL;
			recv(loc_sock, b_mes, MAX_MES_SIZE, 0);
			fflush(stdout);
			printf("%s\n", b_mes);
		}
		if(FD_ISSET(0, &readfds))
		{
			MET3:
			for (i=0; i<=MAX_MES_SIZE; i++)
				mes[i]=NULL;
			i=0;
			sch=0;
			while(((c=getchar())!='\n') && (i<=MAX_MES_SIZE)){
				if (c!=' '){
					mes[i++]=c;
					sch=0;
				}
				else if (i==0)
					continue;
				else if (sch==0){
					mes[i++]=c;
					sch++;
				}
			}
			if(i==MAX_MES_SIZE+1 || i==0 || (i==1 && mes[0]==' ')|| (i==1 && mes[0]=='\t'))
			{
				while((c=getchar())!='\n');
				printf("%s", "\033[1;31m");
				printf("Empty or too big message!\n");
				printf("%s", "\x1B[37m");
				fflush(stdout);
				goto MET3;
			}
			if(mes[i-1]==' ')
				mes[i-1]='\0';
			else
				mes[i]='\0';
			send(loc_sock, mes, strlen(mes)+1, 0);
			if(strncmp("\\quit", mes, 5)==0){
				shutdown(loc_sock, 2);
				return 0;
			}

		}
		if (sigsetjmp(interrupt,1))
		{
			strcpy(mes,"\\quit");
			send(loc_sock,mes,strlen(mes)+1,0);	
			if (loc_sock!=-1) 
				shutdown(loc_sock, 2);;
			return 0;
		}
	}
	shutdown(loc_sock, 2);
	return 0;
}