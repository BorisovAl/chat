#include <sys/types.h>
#include <sys/socket.h> 
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <setjmp.h>

#define MAX_MES_SIZE 1000
#define MAX_NAME 30

typedef struct Node{
	int sock_id;
	char name[MAX_NAME];
	int connect;
	struct Node* next;
} node;

typedef struct Node* info;

sigjmp_buf metka;

void SigHand(int s){
	printf("{SERVER} Serever stopped.\n");
	siglongjmp(metka, 1);
}

void addsoc(int fd, char* nam, info *us_list) {
	info tmp = *us_list;
	if (tmp == NULL) {
		*us_list = malloc(sizeof(node));
		(*us_list)->sock_id = fd;
		strcpy((*us_list)->name, nam);
		(*us_list)->connect=1;
		(*us_list)->next = NULL;
	} 
	else {
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = malloc(sizeof(node));
		tmp = tmp->next;
		tmp->sock_id = fd;
		strcpy(tmp->name, nam);
		tmp->connect=1;
		tmp->next = NULL;
	}
	return;
}

int check_name(char *name, info us_list) {
    while (us_list != NULL) {
        if (strcmp(us_list->name, name) == 0)
            return 0;
        us_list = us_list->next;
    }
    return 1;
}

void delete_sock(info user){
	info tmp = user;
	while (user != NULL){
		tmp = user;
		user = user -> next;
		close(tmp -> sock_id);
		free(tmp);
	}
	return;
}

int main(){
	int port, i;
	info users=NULL;
	info tmp=NULL, ttmp=NULL;
	struct sockaddr_in addr;
	char ent_buf[]=" entered the chat.";
	char ex_buf[]=" left the chat with last message: ";
	char msg[MAX_MES_SIZE];
	char buf[MAX_MES_SIZE+20];
	signal(SIGINT,SigHand);  //для Ctrl+C 
	signal(SIGTSTP,SigHand); // для Ctrl+Z 
	if(sigsetjmp(metka, 1))
		return 0;
	int main_sock=socket(AF_INET, SOCK_STREAM, 0);
	if(sigsetjmp(metka, 1))
	{
		shutdown(main_sock, 2);
		return 0;
	}
	if (main_sock<0)
	{
		perror("{SERVER} Error");
		exit(1);
	}
	printf("{SERVER} Enter the port number: ");
	scanf("%d", &port);
	addr.sin_family = AF_INET; 
	addr.sin_port = htons(port); 
	addr.sin_addr.s_addr = INADDR_ANY;
	if (bind(main_sock,(struct sockaddr *) &addr, sizeof(addr))<0)
	{
		perror("{SERVER} Error");
		exit(2);
	}
	if (listen(main_sock, 5)<0)
		{
			perror("{SERVER} Error");
			exit(3);	
		}
	for (i=0; i<MAX_MES_SIZE; i++)
		msg[i]=NULL;
	while(1)
	{	
		fd_set readfds;
		int max_d=main_sock;
		FD_ZERO(&readfds);
		FD_SET(main_sock, &readfds);
		FD_SET(0,&readfds); 
		for(tmp=users; tmp!=NULL; tmp=tmp->next)
		{
			if(tmp->connect)
			{
				FD_SET(tmp->sock_id, &readfds);
				if(tmp->sock_id>max_d)
					max_d=tmp->sock_id;
			}
		}
		int res=select(max_d+1, &readfds, NULL, NULL, NULL);
		if (res<=0)
		{	
			perror("{SERVER} Error");
			exit(4);
		}
		if(FD_ISSET(main_sock, &readfds))
		{
			int fd=accept(main_sock, NULL, NULL);
			if (fd<0)
				perror("{SERVER} Error");
			else
			{
				recv(fd, msg, MAX_NAME+1, 0);
				if (check_name(msg, users)==0)
					{
						strcpy(buf, "{SERVER} Sorry, this name allready used in chat. Choose another one.");
						send(fd, buf, strlen(buf)+1, 0);
						close(fd);
					}
				else
					{
						addsoc(fd, msg, &users);
						strcpy(buf, msg);
						strcat (buf, ent_buf);
						printf("{SERVER} New user in the chat. Name: %s\n", msg);
						for(tmp=users; tmp!=NULL; tmp=tmp->next)
							{
								if(tmp->connect)
								send(tmp->sock_id, buf, strlen(buf)+1, 0);
							}
					}
			}
		}
		for(tmp=users; tmp!=NULL; tmp=tmp->next)
		{
			if(FD_ISSET(tmp->sock_id, &readfds))
			{
				for (i=0; i<MAX_MES_SIZE; i++)
					msg[i]=NULL;
				for (i=0; i<MAX_MES_SIZE+20; i++)
					buf[i]=NULL;
				recv(tmp->sock_id, msg, MAX_MES_SIZE, 0);
				if(strncmp("\\quit", msg, 5)==0)
				{
					strcpy(buf, tmp->name);
					strcat (buf, ex_buf);
					strcat(buf, &(msg[6]));
					printf("{SERVER} User %s left the chat.\n", tmp->name);
					for(ttmp=users; ttmp!=NULL; ttmp=ttmp->next)
					{
						if(ttmp->connect);
						send(ttmp->sock_id, buf, strlen(buf)+1, 0);
					}
					shutdown(tmp->sock_id, 2);
					tmp->connect=0;
				}
				else if(strncmp("\\users", msg, 6)==0)
				{
					printf("{SERVER} User %s asked to show all the users.\n", tmp->name);
					strcpy(buf, "\nActive users:\n");
					for(ttmp=users; ttmp!=NULL; ttmp=ttmp->next)
					{
						if(ttmp->connect)
						{
							strcat(buf, ttmp->name);
							strcat(buf, "\n");
						}
					}
					send(tmp->sock_id, buf, strlen(buf)+1, 0);
				}
				else if(strcmp("\\help", msg)==0)
				{
					printf("{SERVER} User %s asked to show all the commands.\n", tmp->name);
					strcpy(buf, "\nActive commands:\n");
					strcat(buf, "\\help - to see all commands\n");
					strcat(buf, "\\quit - to leave the chat\n");
					strcat(buf, "\\users - to see active users\n");
					send(tmp->sock_id, buf, strlen(buf)+1, 0);
				}
				else
				{
					strcpy(buf, tmp->name);
					strcat (buf, ": ");
					strcat (buf, msg);
					for(ttmp=users; ttmp!=NULL; ttmp=ttmp->next)
					{
						if(ttmp->connect)
						send(ttmp->sock_id, buf, strlen(buf)+1, 0);
					}
				}
			}
		}
		if(sigsetjmp(metka, 1))
		{
			strcpy(buf,"{SERVER} Server stopped.");
			for(ttmp=users; ttmp!=NULL; ttmp=ttmp->next)
				{
					if(ttmp->connect)
						send(ttmp->sock_id, buf, strlen(buf)+1, 0);
				}
			delete_sock(users);
			shutdown(main_sock, 2);
			return 0;
		}
	}
	delete_sock(users);
	shutdown(main_sock, 2);
	return 0;
}